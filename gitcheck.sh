#!/bin/bash
# Git checker - A script to recursively find and check the status of Git repositories
# Copyright (C) 2016-2018 Tulir Asokan

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

export SHOW_STATUS=false

function checkdir {
	if [[ -f ".gitcheckignore" ]]; then
		return
	fi

	for file in *; do
		if [[ -d "$file" && ! -L "$file" ]]; then
			cd "$file" > /dev/null
			if [[ -d ".git" ]]; then
				unpushed=$(git status | grep "Your branch is up to date with")
				uncommitted=$(git status | grep "nothing to commit, working tree clean")

				if [ -z "$uncommitted" ]; then
					if [ -z "$unpushed" ]; then
						echo "Unpushed and uncommitted changes in $(pwd)"
						$SHOW_STATUS && git status
					else
						echo "Uncommitted changes in $(pwd)"
						$SHOW_STATUS && git status
					fi
				elif [ -z "$unpushed" ]; then
					echo "Unpushed changes in $(pwd)"
					$SHOW_STATUS && git status
				fi
			else
				checkdir .
			fi
			cd .. > /dev/null
		fi
	done
}

while getopts "sh" opt; do
	case "$opt" in
		h) shopt -s dotglob;;
		s) SHOW_STATUS=true;;
	esac
done

checkdir "$1"